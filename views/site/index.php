<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Selamat Datang!</h1>

        <p class="lead">Anda Berhasil Membuat Halaman Frontend Web API.</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <h2>Hai!</h2>

                <p>Halaman Frontend ini terkoneksi ke Web Service.</p>
            </div>
            <div class="col-lg-6">                
                <h2>Config</h2>
                <div class="container">
                <p>#URL API<br>
                <code><?=Yii::$app->params['url_api']?></code><br>
                <small>untuk mengubahnya di file "/config/params.php"</small>
                </p>
                </div>

                <div class="container">
                <p>#User API<br>
                <code><?=Yii::$app->params['user_api']?></code><br>
                <small>untuk mengubahnya di file "/config/params.php"</small>
                </p>
                </div>

                <div class="container">
                <p>#Password API<br>
                <code>******</code><br>
                <small>untuk mengubahnya di file "/config/params.php"</small>
                </p>
                </div>

            </div>
            <div class="col-lg-6">                
                <h2>Petunjuk</h2>
                <div class="container">
                <ul>
                <li>Setiap Controller yang digunakan wajib "extends" ke MainController</li>
                <li>Setiap data diambil dari webservice. Contoh Controller dapat dilihat di "ExampleController"</li>
                </ul>
                </p>
                </div>
            </div>
        </div>

    </div>
</div>
