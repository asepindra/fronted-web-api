<?php 
	use yii\helpers\Url;
	use yii\bootstrap4\ActiveForm;
	$this->title = 'Ubah Mahasiswa';
	$this->params['breadcrumbs'][] = ['url' => ['data-mahasiswa'], 'label' => 'Mahasiswa'];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<?php ActiveForm::begin(['action' => ['update-mahasiswa', 'id' => $id]]) ?>
					<div class="form-group">
						<label>Nim</label>
						<input type="text" name="nim" class="form-control" value="<?= $data->nim ?>">
					</div>
					<div class="form-group">
						<label>Nama</label>
						<input type="text" name="name" class="form-control" value="<?= $data->name ?>">
					</div>
					<div class="form-group">
						<label>Angkatan</label>
						<input type="text" name="angkatan" class="form-control" value="<?= $data->angkatan ?>">
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary">
							Simpan data
						</button>
					</div>
				<?php ActiveForm::end() ?>
			</div>
		</div>
	</div>
</div>
