<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap5\ActiveForm $form */
/** @var app\models\ContactForm $model */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

$this->title = 'Contoh Pengambilan Data Mahasiswa';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $data,
        'columns' => [
            'id',
            'nim',
            'name',
            'angkatan',
            [
                'label'=>'Aksi',
                'format'=>'raw',
                'value'=> function($model){
                    return '<a href="'.Url::to(['update-mahasiswa','id'=>$model->id]).'" class="btn btn-warning">Update</a>';
                }
            ]
        ],
    ]) ?>

</div>
