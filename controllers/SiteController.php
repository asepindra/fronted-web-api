<?php

namespace app\controllers;

use Yii;
// use yii\web\Controller;
use app\models\LoginForm;


class SiteController extends MainController
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        // $getParams = ['access-token'=> $this->getToken()];
        // $data = $this->sendApi('/mahasiswa/index', 'GET', $getParams,[]);

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $session = Yii::$app->session;
        $this->getToken();
        return $this->render('about');
    }
}
