<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class ExampleController extends MainController
{
    // public function behaviors()
    // {
    //     return [
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             // 'only' => ['logout'],
    //             'rules' => [
    //                 [
    //                     'actions' => ['logout'],
    //                     'allow' => true,
    //                     'roles' => ['@'],
    //                 ],
    //             ],
    //            'denyCallback' => function($rule, $action) {
    //              Yii::$app->response->redirect(['site/login']); 
    //              },
    //         ],
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'logout' => ['post'],
    //             ],
    //         ],
    //     ];
    // }
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $getParams = ['access-token'=> $this->getToken()];
        $data = $this->sendApi('/mahasiswa/index', 'GET', $getParams,[]);

        return $this->render('get_mahasiswa',['data'=>$data]);
    }

    public function actionTambahMahasiswa()
    {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $postData = [
                'name' => $post['name'],
                'nim' => $post['nim'],
                'angkatan' => $post['angkatan'],
            ];

            $getParams = ['access-token'=> $this->getToken()];

            $data = $this->sendApi('/mahasiswa/create','POST',$getParams,$postData);
            return $this->redirect('index');

        }

        return $this->render('create_mahasiswa', [
            'data' => [
                'nim' => '',
                'name' => '',
                'angkatan' => '',
            ]
        ]);
    }

    public function actionUpdateMahasiswa($id)
    {
        $getParams = ['access-token'=> $this->getToken()];
        
        if (Yii::$app->request->isPost) {

            $post = Yii::$app->request->post();
            $getParams = [
                'access-token'=> $this->getToken(),
                'id' => $id
            ];
            $postData = [
                'nim' => $post['nim'],
                'name' => $post['name'],
                'angkatan' => $post['angkatan'],
            ];

            $data = $this->sendApi('mahasiswa/update','PUT',$getParams, $postData);
            return $this->redirect('index');

        }

        $response = $this->sendApi('mahasiswas/'.$id,'GET',$getParams);

        if(isset($response->name) && $response->name == 'Not Found'){
            Yii::$app->session->setFlash('error', 'Data Mahasiswa Tidak Ditemukan');

            return $this->redirect('index');
        }

        return $this->render('update_mahasiswa', ['data' => $response, 'id' => $id]);
    }

    public function actionDelete($id)
    {
        $curl = new curl\Curl();
        $response = $curl->setGetParams([
            'access-token' => Yii::$app->params['token'],
        ])->delete($this->urlApi('admins/'.$id));
        if ($curl->errorCode === null) {
            if ($curl->responseCode != 204) {
                throw new NotFoundHttpException('Halaman tidak ditemukan');
            }
        }
        return $this->redirect(['data-admin']);
    }   

}