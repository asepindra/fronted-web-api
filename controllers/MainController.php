<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use linslin\yii2\curl;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;

class MainController extends Controller
{
	
	/**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function urlApi($action)
	{
		return Yii::$app->params['url_api'].'/'.$action;
	}

	public function getToken()
	{
		$session = Yii::$app->session;

		if(!is_null($session->get('access-token')))
			return $session->get('access-token');
		else
			$this->apiLogin();
	}

	public function apiLogin()
	{
		$session = Yii::$app->session;
		$curl = new curl\Curl();
        $postData = [
            'username' => Yii::$app->params['user_api'],
            'password' => Yii::$app->params['password_api'],
        ];

        $response = $curl->setPostParams($postData)->post($this->urlApi('/site/login'));
        if ($curl->errorCode === null) {
            $response = json_decode($response);
            if(isset($response->user)){
            	$session->set('access-token',$response->user);
            	$this->getToken();
            	return true;
            }
            else
            	return false;
            	// throw new \yii\web\HttpException('Login Gagal');
        }
        return false;
	}

	public function sendApi($action,$method = 'GET',$getParams = array(),$postParams =array())
	{
        $curl = new curl\Curl();
        $data = new ArrayDataProvider();
        $response = $curl;

        if(count($postParams))
        	$response = $response->setPostParams($postParams);

        if(count($getParams))
        	$response = $response->setGetParams($getParams);


        if($method == 'GET'){    	
	        $response = $response->get($this->urlApi($action));
        }
        elseif($method == 'POST'){
	        $response = $response->post($this->urlApi($action));
        }
        elseif($method == 'PUT'){
	        $response = $response->put($this->urlApi($action));
        }
        elseif($method == 'DELETE'){
	        $response = $response->delete($this->urlApi($action));
        }

        if ($curl->errorCode === null) {
            $response = json_decode($response);
            // var_dump($response);die();
            if(isset($response->status) && $response->status == 401 && $this->apiLogin()){
            	// $this->sendApi($action,$method,$getParams,$postParams);
            	die('Gagal login, Silakan Refresh Kembali');
            }
            if(isset($response->items))
            	$data = new ArrayDataProvider(['allModels' => $response->items]);
            else
            	$data = $response;

        }
        else {
        	$data = [];
            // Yii::$app->session->setFlash('error', $response->status.': '.$response->message);
        }

        return $data;

	}
}
